package com.exam.improvingtest.base

import androidx.annotation.StringRes
import androidx.fragment.app.Fragment

abstract class BaseFragment : Fragment(), BaseAndroidProvider {

    val baseActivity by lazy {
        requireActivity() as BaseActivity
    }

    override fun showError(@StringRes messageRes: Int) {
        baseActivity.showError(messageRes)
    }

    override fun showError(message: String) {
        baseActivity.showError(message)
    }

    override fun showMessage(message: String, iconSuccessAlert: Int?) {
        baseActivity.showMessage(message, iconSuccessAlert)
    }

    override fun showMessage(@StringRes messageRes: Int) {
        baseActivity.showMessage(messageRes)
    }

    override fun showMessage(message: String) {
        baseActivity.showMessage(message)
    }

    override fun showLoading(show: Boolean) {
        baseActivity.showLoading(show)
    }

    override fun dismissProgressDialog() {
        baseActivity.dismissProgressDialog()
    }

    override fun showProgressDialog() {
        baseActivity.showProgressDialog()
    }
}