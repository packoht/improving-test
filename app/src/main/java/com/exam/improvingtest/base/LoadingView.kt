package com.exam.improvingtest.base

import android.content.Context
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.util.AttributeSet
import android.view.Gravity
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.exam.improvingtest.R

class LoadingView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    private val textView by lazy {
        val view = TextView(context)
        view.setPadding(0, resources.getDimensionPixelSize(R.dimen.spacing_medium), 0, 0)
        view
    }

    init {
        isClickable = true
        orientation = VERTICAL
        layoutParams = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        gravity = Gravity.CENTER
        setBackgroundResource(R.color.purple_200)

        val layoutParamsViews =
            LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        layoutParamsViews.gravity = Gravity.CENTER

        val progressBar = ProgressBar(context)

        val colorFilter = PorterDuffColorFilter(
            ContextCompat.getColor(context, R.color.purple_200),
            PorterDuff.Mode.MULTIPLY
        )

        progressBar.indeterminateDrawable.colorFilter = colorFilter

        addView(progressBar, layoutParamsViews)
        addView(textView, layoutParamsViews)
    }

    fun setMessage(message: String) {
        textView.text = message
    }
}