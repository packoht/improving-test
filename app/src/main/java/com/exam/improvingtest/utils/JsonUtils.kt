package com.exam.improvingtest.utils

import android.content.res.Resources
import com.google.gson.GsonBuilder
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.StringWriter

fun <T> getJsonFileAsClass(resources: Resources, resId: Int, classType: Class<T>): T? {
    val resourceReader = resources.openRawResource(resId)
    val writer = StringWriter()

    tryWithFinally(
        {
            val reader = BufferedReader(InputStreamReader(resourceReader, "UTF-8"))
            var line: String? = reader.readLine()
            while (line != null) {
                writer.write(line.orEmpty())
                line = reader.readLine()
            }
            return constructUsingGson(classType, writer.toString())
        },
        {
            tryOrPrintException {
                resourceReader.close()
            }
        }
    )
    return null
}

private fun <T> constructUsingGson(type: Class<T>, jsonString: String): T {
    val gson = GsonBuilder().create()
    return gson.fromJson(jsonString, type)
}
