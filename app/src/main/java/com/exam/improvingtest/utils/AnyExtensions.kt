package com.exam.improvingtest.utils

@Suppress("TooGenericExceptionCaught")
inline fun tryWithFinally(f: () -> Unit, finally: () -> Unit) {
    return try {
        f()
    } catch (e: Exception) {
        e.printStackTrace()
    } finally {
        finally()
    }
}

@Suppress("TooGenericExceptionCaught")
inline fun tryOrPrintException(f: () -> Unit) {
    return try {
        f()
    } catch (e: Exception) {
        e.printStackTrace()
    }
}