package com.exam.improvingtest.di.component

import com.exam.improvingtest.application.InjectableApplication
import com.exam.improvingtest.di.module.ExamAppModule
import com.exam.improvingtest.di.module.ExamModule
import com.exam.improvingtest.di.module.ExamenActivityBuilder
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        ExamenActivityBuilder::class,
        ExamAppModule::class,
        ExamModule::class,
        AndroidSupportInjectionModule::class
    ]
)
interface MainComponent : BaseComponent {

    @Component.Builder
    interface Builder {
        fun build(): MainComponent

        @BindsInstance
        fun application(application: InjectableApplication): Builder
    }
}