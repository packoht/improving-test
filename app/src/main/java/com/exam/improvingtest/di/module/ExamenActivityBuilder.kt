package com.exam.improvingtest.di.module

import com.exam.improvingtest.flow.home.HomeActivity
import com.exam.improvingtest.scopes.ActivityScope
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ExamenActivityBuilder {

    @ActivityScope
    @ContributesAndroidInjector(
        modules = [
            HomeFragmentsModule::class
        ]
    )
    abstract fun bindHomeActivity (): HomeActivity
}