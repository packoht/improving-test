package com.exam.improvingtest.di.module

import com.exam.improvingtest.flow.home.fragments.DetailLevelFragment
import com.exam.improvingtest.flow.home.fragments.MainListItemsFragment
import com.exam.improvingtest.scopes.FragmentScope
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class HomeFragmentsModule {

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributesMainListItemsFragment(): MainListItemsFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributesDetailLevelFragment(): DetailLevelFragment

}