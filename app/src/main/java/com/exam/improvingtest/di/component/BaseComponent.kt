package com.exam.improvingtest.di.component

import android.app.Application
import android.content.Context
import com.exam.improvingtest.application.InjectableApplication
import com.google.gson.Gson
import dagger.android.AndroidInjector

interface BaseComponent : AndroidInjector<InjectableApplication> {

    fun context(): Context

    fun application(): Application

    fun gson(): Gson
}