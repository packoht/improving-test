package com.exam.improvingtest.di.module

import android.app.Application
import android.content.Context
import com.exam.improvingtest.application.InjectableApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object ExamAppModule {

    @Provides
    @Singleton
    fun providesContext(app: InjectableApplication): Context = app.applicationContext

    @Provides
    @Singleton
    fun providesApplication(app: InjectableApplication): Application = app
}