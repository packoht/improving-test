package com.exam.improvingtest.repository

import android.content.Context
import com.exam.improvingtest.R
import com.exam.improvingtest.flow.home.models.ItemDataModel
import com.exam.improvingtest.flow.home.models.Local
import com.exam.improvingtest.flow.home.models.LocalesModel
import com.exam.improvingtest.utils.getJsonFileAsClass
import dagger.Reusable
import javax.inject.Inject

@Reusable
class MainListRepository @Inject constructor(
    private val context: Context
) {
    val countryCodes: List<ItemDataModel> by lazy { loadCountryCodes() }
    val locales: LocalesModel? by lazy { loadLocales() }

    private fun loadCountryCodes(): List<ItemDataModel> {
        return getJsonFileAsClass(
            context.resources,
            R.raw.test_data,
            Array<ItemDataModel>::class.java
        )?.toList().orEmpty()
    }

    private fun loadLocales(): LocalesModel? {
        return getJsonFileAsClass(
            context.resources,
            R.raw.locales,
            LocalesModel::class.java
        )
    }
}