package com.exam.improvingtest.flow.home.models

enum class LevelEnum(val level: String) {
    GOLD("gold"),
    SILVER("silver"),
    BRONZE("bronze")
}

enum class Local(val local: String) {
    MX("mx"),
    PH("ph"),
    KE("ke")
}

enum class Status(val status: String) {
    PAID("paid"),
    DUE("due"),
    APPROVED("approved")
}