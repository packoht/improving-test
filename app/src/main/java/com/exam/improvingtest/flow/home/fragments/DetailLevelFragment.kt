package com.exam.improvingtest.flow.home.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.exam.improvingtest.base.BaseFragment
import com.exam.improvingtest.databinding.FragmentDetailLevelBinding
import com.exam.improvingtest.flow.home.viewModel.DetailLevelViewModel
import com.exam.improvingtest.utils.viewBinding
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

class DetailLevelFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: DetailLevelViewModel

    val args: DetailLevelFragmentArgs by navArgs()

    private val binding by viewBinding {
        FragmentDetailLevelBinding.inflate(layoutInflater)
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.setConfigView(args.itemSelected)
    }
}