package com.exam.improvingtest.flow.home

import android.os.Bundle
import androidx.navigation.NavDirections
import androidx.navigation.findNavController
import com.exam.improvingtest.R
import com.exam.improvingtest.base.BaseActivity
import com.exam.improvingtest.flow.home.fragments.MainListItemsFragment
import com.exam.improvingtest.flow.home.fragments.MainListItemsFragmentDirections
import com.exam.improvingtest.flow.home.models.ItemDataModel
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

class HomeActivity :
    BaseActivity(),
    HasAndroidInjector,
    MainListItemsFragment.MainListListener {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    override fun androidInjector(): AndroidInjector<Any> = dispatchingAndroidInjector

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
    }

    fun navigateTo(direction: NavDirections) =
        findNavController(R.id.navHostFragment).navigate(direction)

    override fun onItemSelected(itemSelected: ItemDataModel) {
        navigateTo(MainListItemsFragmentDirections.actionToDetailFragment(itemSelected))
    }
}