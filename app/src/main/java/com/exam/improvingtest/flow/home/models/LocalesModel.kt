package com.exam.improvingtest.flow.home.models

import com.google.gson.annotations.SerializedName

data class LocalesModel(
    @SerializedName("ke") val ke: Local,
    @SerializedName("mx") val mx: Local,
    @SerializedName("ph") val ph: Local
){
    data class Local(
        @SerializedName("currency") val currency: String,
        @SerializedName("loanLimit") val loanLimit: Long,
        @SerializedName("timezone") val timezone: Long
    )
}