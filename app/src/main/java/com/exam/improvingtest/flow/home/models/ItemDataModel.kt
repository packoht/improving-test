package com.exam.improvingtest.flow.home.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ItemDataModel(
    @SerializedName("locale") val local: String,
    @SerializedName("loan") val loan: Loan?,
    @SerializedName("timestamp") val timestamp: Long,
    @SerializedName("username") val username: String
) : Parcelable {

    @Parcelize
    data class Loan(
        @SerializedName("status") val status: String?,
        @SerializedName("level") val level: String?,
        @SerializedName("due") val due: Long?,
        @SerializedName("dueDate") val dueDate: Long?,
        @SerializedName("approved") val approved: Long?
    ) : Parcelable
}