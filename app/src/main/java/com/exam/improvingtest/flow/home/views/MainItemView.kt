package com.exam.improvingtest.flow.home.views

import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import com.exam.improvingtest.R
import com.exam.improvingtest.databinding.ItemMainListBinding
import com.exam.improvingtest.flow.home.models.LevelEnum
import com.exam.improvingtest.flow.home.models.ItemDataModel
import com.xwray.groupie.viewbinding.BindableItem

@SuppressLint("UseCompatLoadingForDrawables")
class MainItemView(
    val item: ItemDataModel,
    val context: Context
) : BindableItem<ItemMainListBinding>() {

    override fun bind(viewBinding: ItemMainListBinding, position: Int) {
        viewBinding.apply {
            textViewUser.text = item.username
            imageViewLevel.background = getImage(item.loan?.level.orEmpty())
        }
    }

    private fun getImage(level: String) = context.getDrawable(
        when (level) {
            LevelEnum.BRONZE.level -> R.drawable.img_bronze_badge_large
            LevelEnum.GOLD.level -> R.drawable.img_gold_badge_large
            LevelEnum.SILVER.level -> R.drawable.img_silver_badge_large
            else -> R.drawable.img_blue_badge_large
        }
    )

    override fun getLayout() = R.layout.item_main_list

    override fun initializeViewBinding(view: View) = ItemMainListBinding.bind(view)
}