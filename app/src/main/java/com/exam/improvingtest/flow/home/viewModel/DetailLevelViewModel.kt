package com.exam.improvingtest.flow.home.viewModel

import androidx.lifecycle.MutableLiveData
import com.exam.improvingtest.R
import com.exam.improvingtest.base.BaseViewModel
import com.exam.improvingtest.flow.home.models.ItemDataModel
import com.exam.improvingtest.flow.home.models.LevelEnum
import com.exam.improvingtest.flow.home.models.Local
import com.exam.improvingtest.flow.home.models.Status
import com.exam.improvingtest.repository.MainListRepository
import com.exam.improvingtest.scopes.ActivityScope
import com.exam.improvingtest.utils.ResourceProvider
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import javax.inject.Inject

@ActivityScope
class DetailLevelViewModel @Inject constructor(
    val resources: ResourceProvider,
    private val repository: MainListRepository
) : BaseViewModel() {

    val appliedImageView = MutableLiveData(resources.getDrawable(R.drawable.img_loan_status_apply))
    val appliedTextTitle = MutableLiveData("")
    val appliedTextSubtitle = MutableLiveData("")
    val isAppliedVisible = MutableLiveData(true)

    val paymentTitleText = MutableLiveData("")
    val paymentDescriptionText = MutableLiveData("")

    val talaStatusTextTitle =
        MutableLiveData(resources.getString(R.string.fragment_detail_level_status_title))
    val talaStatusTextsubtitle = MutableLiveData("")
    val talaStatusImageView =
        MutableLiveData(resources.getDrawable(R.drawable.img_gold_badge_large))

    val readMoreTextTitle =
        MutableLiveData(resources.getString(R.string.framgnet_detail_level_read_more_title))
    val readMoreTextSubtitle =
        MutableLiveData(resources.getString(R.string.framgnet_detail_level_read_more_subtitle))
    val readMoreImageView = MutableLiveData(resources.getDrawable(R.drawable.img_story_card_ke))

    private var _itemSelected: ItemDataModel? = null

    fun setConfigView(itemSelected: ItemDataModel) {
        this._itemSelected = itemSelected

        initConfigView()
    }

    private fun initConfigView() {
        setAppliedImage()
        setApplyTextTitle()
        setApplyTextSubtitle()

        setStatusImage()
        setReadMoreImage()
        setStatusSubtitle()
    }

    private fun setAppliedImage() {
        if (_itemSelected?.loan?.status == Status.DUE.status) {
            isAppliedVisible.value = false
            setDuePayment()
        } else {
            appliedImageView.value = resources.getDrawable(
                when (_itemSelected?.loan?.status) {
                    Status.APPROVED.status -> R.drawable.img_loan_status_approved
                    Status.PAID.status -> R.drawable.img_loan_status_paidoff
                    else -> R.drawable.img_loan_status_apply
                }
            )
        }
    }

    private fun setDuePayment() {
        paymentTitleText.value = getDueAmount()
        paymentDescriptionText.value = resources.getString(
            R.string.fragment_detail_repay_due_date,
            formatDate(_itemSelected?.loan?.dueDate)
        )
    }

    private fun setApplyTextTitle() {
        if (_itemSelected?.loan?.status != Status.DUE.status) {
            appliedTextTitle.value = resources.getString(
                when (_itemSelected?.loan?.status) {
                    Status.APPROVED.status -> R.string.fragment_detail_level_applied_approved_title
                    Status.PAID.status -> R.string.fragment_detail_level_applied_paidoff_title
                    else -> R.string.fragment_detail_level_applied_apply_title
                }
            )
        }
    }

    private fun setApplyTextSubtitle() {
        if (_itemSelected?.loan?.status != Status.DUE.status) {
            appliedTextSubtitle.value = when (_itemSelected?.loan?.status) {
                Status.APPROVED.status -> resources.getString(
                    R.string.fragment_detail_level_applied_approved_subtitle,
                    getAmountApproved(_itemSelected?.loan?.approved!!)
                )
                Status.PAID.status -> resources.getString(R.string.fragment_detail_level_applied_paidoff_subtitle)
                else -> resources.getString(
                    R.string.fragment_detail_level_applied_apply_subtitle,
                    getMaxOfferApply()
                )
            }
        }
    }

    private fun setStatusSubtitle() {
        talaStatusTextsubtitle.value =
            when (_itemSelected?.loan?.level) {
                LevelEnum.BRONZE.level,
                LevelEnum.SILVER.level,
                LevelEnum.GOLD.level -> resources.getString(
                    R.string.fragment_detail_level_status_new_subtitle_locale,
                    getMaxOfferApply()
                )
                else -> resources.getString(R.string.fragment_detail_level_status_new_subtitle)
            }
    }

    private fun setStatusImage() {
        talaStatusImageView.value = resources.getDrawable(
            when (_itemSelected?.loan?.level) {
                LevelEnum.BRONZE.level -> R.drawable.img_bronze_badge_large
                LevelEnum.SILVER.level -> R.drawable.img_silver_badge_large
                LevelEnum.GOLD.level -> R.drawable.img_gold_badge_large
                else -> R.drawable.img_blue_badge_large
            }
        )
    }

    private fun setReadMoreImage() {
        readMoreImageView.value = resources.getDrawable(
            when (_itemSelected?.local) {
                Local.MX.local -> R.drawable.img_story_card_mx
                Local.KE.local -> R.drawable.img_story_card_ke
                Local.PH.local -> R.drawable.img_story_card_ph
                else -> R.drawable.img_story_card_mx
            }
        )
    }

    fun onInviteFriends() {
    }

    fun onViewFaqs() {
    }

    private fun getCurrency(): String {
        val local = _itemSelected?.local
        val remoteLocales = repository.locales
        return when (local) {
            Local.PH.local -> remoteLocales?.ph
            Local.KE.local -> remoteLocales?.ke
            else -> remoteLocales?.mx
        }?.currency.orEmpty()
    }

    private fun getMaxOffer(): Long {
        val local = _itemSelected?.local
        val remoteLocales = repository.locales
        return when (local) {
            Local.PH.local -> remoteLocales?.ph
            Local.KE.local -> remoteLocales?.ke
            else -> remoteLocales?.mx
        }?.loanLimit ?: 0x0
    }

    private fun getAmountApproved(amount: Long) = "${getCurrency()} $amount"

    private fun getMaxOfferApply() = "${getCurrency()} ${getMaxOffer()}"

    private fun getDueAmount() = "${getCurrency()} ${_itemSelected?.loan?.due}"

    fun formatDate(
        date: Long?,
        formatTo: String = "$DATE_FORMAT_SEPARATOR_SLASH $HOUR_FORMAT_24"
    ): String {
        return date?.let {
            SimpleDateFormat(formatTo, Locale.getDefault()).format(Date(it))
        } ?: ""
    }

    private companion object {
        const val DATE_FORMAT_SEPARATOR_SLASH = "dd/MM/yyyy"
        const val HOUR_FORMAT_24 = "kk:mm"
    }
}