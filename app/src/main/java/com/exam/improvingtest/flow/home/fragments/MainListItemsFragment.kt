package com.exam.improvingtest.flow.home.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.exam.improvingtest.base.BaseFragment
import com.exam.improvingtest.databinding.FragmentMainListBinding
import com.exam.improvingtest.flow.home.models.ItemDataModel
import com.exam.improvingtest.flow.home.viewModel.MainListViewModel
import com.exam.improvingtest.flow.home.views.MainItemView
import com.exam.improvingtest.utils.viewBinding
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import dagger.android.support.AndroidSupportInjection
import java.util.Date
import javax.inject.Inject

class MainListItemsFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: MainListViewModel

    private lateinit var adater: GroupAdapter<GroupieViewHolder>

    private var listener: MainListListener? = null

    private val binding by viewBinding {
        FragmentMainListBinding.inflate(layoutInflater)
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
        listener = context as? MainListListener
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        Date().time
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindViewModel()
        initRecyclerView()
    }

    private fun initRecyclerView() {
        adater = GroupAdapter()
        binding.recyclerMainItemList.apply {
            layoutManager = LinearLayoutManager(requireContext())
            setHasFixedSize(true)
            adapter = adater
        }
        adater.setOnItemClickListener { itemSelected, _ ->
            if (itemSelected is MainItemView) {
                listener?.onItemSelected(itemSelected.item)
            }
        }
    }

    private fun bindViewModel() {
        viewModel.getListItems().observe(viewLifecycleOwner, ::handleListItems)
    }

    private fun handleListItems(listItems: List<ItemDataModel>) {
        adater.clear()
        adater.addAll(
            listItems.map {
                MainItemView(
                    it, requireContext()
                )
            }
        )
    }

    interface MainListListener {
        fun onItemSelected(itemSelected: ItemDataModel)
    }
}