package com.exam.improvingtest.flow.home.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.exam.improvingtest.base.BaseViewModel
import com.exam.improvingtest.flow.home.models.ItemDataModel
import com.exam.improvingtest.repository.MainListRepository
import javax.inject.Inject

class MainListViewModel @Inject constructor(
    private val repository: MainListRepository
) : BaseViewModel() {

    private val lisItems = MutableLiveData<List<ItemDataModel>>()
    fun getListItems(): LiveData<List<ItemDataModel>> = lisItems

    init {
        lisItems.value = repository.countryCodes
    }
}